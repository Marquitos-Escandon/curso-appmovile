import { NgModule, NO_ERRORS_SCHEMA } from "@angular/core";
import { NativeScriptCommonModule } from "@nativescript/angular";
import { NoticiasService } from "../domain/noticias.service";

import { SearchRoutingModule } from "./search-routing.module";
import { SearchComponent } from "./search.component";

@NgModule({
    imports: [
        NativeScriptCommonModule,
        SearchRoutingModule
    ],
    declarations: [
        SearchComponent
    ],
    // providers: [NoticiasService], // Se pone aca para que mas de un modulo puedo utilizarlo

    schemas: [
        NO_ERRORS_SCHEMA
    ]
})
export class SearchModule { }
